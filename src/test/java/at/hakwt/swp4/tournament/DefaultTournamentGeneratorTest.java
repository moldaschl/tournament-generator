package at.hakwt.swp4.tournament;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DefaultTournamentGeneratorTest {

    @Test
    public void testNumberOfTeams() {

        TournamentGenerator generator = new DefaultTournamentGenerator();
        for (int i = 2; i <= 20; i+=1) {
            // Arrange
            List<Team> teams = new ArrayList<>();
            for (int j = 1; j <= i; j++) {
                teams.add(new Team(""+j));
            }
            // Act
            List<Game> games = generator.generateGames(teams);
            // Assert
            if ( i % 2 == 0 ) {
                assertEquals((i - 1) * i / 2, games.size());
            } else {
                assertEquals((i + 1) * i / 2, games.size());
            }
        }
    }

    @Test
    public void testNumberOfGamesPerTeam() {
        TournamentGenerator generator = new DefaultTournamentGenerator();
        for (int i = 2; i <= 20; i+=2) {
            // Arrange
            List<Team> teams = new ArrayList<>();
            for (int j = 1; j <= i; j++) {
                teams.add(new Team(""+j));
            }

            // Act
            List<Game> games = generator.generateGames(teams);

            // Assert
            Map<Team, Integer> actual = new HashMap<>();
            for(Game g : games) {
                for(Team t : Arrays.asList(g.getAwayTeam(), g.getHomeTeam())) {
                    if ( actual.containsKey(t) ) {
                        actual.put(t, actual.get(t)+1);
                    } else {
                        actual.put(t, 1);
                    }
                }
            }
            assertEquals(i, actual.size());
            for(Map.Entry<Team, Integer> e : actual.entrySet()) {
                assertEquals(i-1, e.getValue());
            }
        }
    }

    @Test
    public void testMatchdays() {
        TournamentGenerator generator = new DefaultTournamentGenerator();
        for (int i = 4; i <= 20; i+=2) {
            // Arrange
            List<Team> teams = new ArrayList<>();
            for (int j = 1; j <= i; j++) {
                teams.add(new Team(""+j));
            }

            // Act
            List<Matchday> matchdays = generator.generate(teams);

            // Assert
            assertEquals(i-1, matchdays.size());
            for(Matchday md : matchdays) {
                Map<Team, Integer> actual = new HashMap<>();
                for(Game g : md.getGames()) {
                    for(Team t : Arrays.asList(g.getAwayTeam(), g.getHomeTeam())) {
                        if ( actual.containsKey(t) ) {
                            actual.put(t, actual.get(t)+1);
                        } else {
                            actual.put(t, 1);
                        }
                    }
                }
                assertEquals(i, actual.size());
                for(Map.Entry<Team, Integer> e : actual.entrySet()) {
                    assertEquals(1, e.getValue());
                }
            }
        }
    }


}
