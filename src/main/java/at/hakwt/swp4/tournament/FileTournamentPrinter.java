package at.hakwt.swp4.tournament;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class FileTournamentPrinter implements TournamentPrinter {

    private final String fileName;

    public FileTournamentPrinter(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void print(List<Game> games) {
        try {
            PrintWriter printWriter = new PrintWriter(this.fileName);
            for(Game g : games) {
                printWriter.println(g.toString());
            }
            printWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
