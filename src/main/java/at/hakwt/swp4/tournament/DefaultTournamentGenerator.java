package at.hakwt.swp4.tournament;

import java.util.ArrayList;
import java.util.List;

public class DefaultTournamentGenerator implements TournamentGenerator {

    @Override
    public List<Matchday> generate(List<Team> givenTeams) {
        List<Matchday> matchdays = new ArrayList<>();

        // create new list of teams because items will be change in this list
        List<Team> teams = new ArrayList<>(givenTeams);
        if (givenTeams.size() % 2 == 1) {
            teams.add(new Team("SPIELFREI"));
        }
        Team joker = teams.remove(teams.size()-1);
        for(int i = 0; i < teams.size(); i++) {
            List<Game> games = new ArrayList<>();
            int j = 0;
            while (j < teams.size() / 2) {
                games.add(new Game(teams.get(j), teams.get(teams.size()-1-j)));
                j++;
            }
            games.add(new Game(teams.get(j), joker));
            teams.add(0, teams.remove(teams.size()-1));
            matchdays.add(new Matchday(i+1, games));
        }
        return matchdays;
    }

    @Override
    public List<Game> generateGames(List<Team> givenTeams) {
        List<Matchday> matchdays = generate(givenTeams);
        List<Game> allGames = new ArrayList<>();
        for(Matchday md : matchdays) {
            allGames.addAll(md.getGames());
        }
        return allGames;
    }

}
