package at.hakwt.swp4.tournament;

import java.util.List;

public class Matchday {

    private final List<Game> games;
    private final int dayNo;

    public Matchday(int dayNo, List<Game> games) {
        this.dayNo = dayNo;
        this.games = games;
    }

    public int getDayNo() {
        return dayNo;
    }

    public List<Game> getGames() {
        return games;
    }
}
