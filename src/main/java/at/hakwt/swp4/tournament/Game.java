package at.hakwt.swp4.tournament;

public class Game {

    private final Team homeTeam;

    private final Team awayTeam;

    public Game(Team homeTeam, Team awayTeam) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    @Override
    public String toString() {
        return homeTeam + " : " + awayTeam;
    }
}
