package at.hakwt.swp4.tournament;

import java.util.List;

public class ConsoleTournamentPrinter implements TournamentPrinter {

    @Override
    public void print(List<Game> games) {
        // games.forEach(System.out::println);
        for(Game g: games) {
            System.out.println(g);
        }
    }

}
