package at.hakwt.swp4.tournament;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        TournamentGenerator generator = applicationContext.getBean("tournamentGenerator", TournamentGenerator.class);

        Team a = new Team("A");
        Team b = new Team("B");
        Team c = new Team("C");
        Team d = new Team("D");
        Team e = new Team("E");

        TournamentPrinter printer = applicationContext.getBean("tournamentPrinter", TournamentPrinter.class);
        List<Game> games = generator.generateGames(Arrays.asList(a, b, c, d, e));
        printer.print(games);
    }

}
