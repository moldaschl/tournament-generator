package at.hakwt.swp4.tournament;

import java.util.List;

public interface TournamentPrinter {

    void print(List<Game> games);

}
