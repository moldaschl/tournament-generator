package at.hakwt.swp4.tournament;

import java.util.List;

public interface TournamentGenerator {
    List<Matchday> generate(List<Team> teams);

    List<Game> generateGames(List<Team> teams);
}
